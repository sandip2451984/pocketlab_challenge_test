<?php 
// So that we may not run out memory in case of some HUGE log file.
ini_set("memory_limit", -1);
set_time_limit(0);
// include LogReader file.
include_once('LogReader.php');
// Default values, if the script is being accessed through the browser
$filePath = 'sample.log';
$separator = "<br>";
// If the invocation is through CLI
if ( php_sapi_name() === 'cli' ) {
	
	// Change the separator for CLI
	$separator = PHP_EOL;
	// If the path to the log file is provided through CLI
	if ( isset( $argv[1] ) ) {
		$filePath = $argv[1];
	}
}

// call logreader class
$reader = new LogReader( $filePath );

// Get data for each of the required URLs (in string format)
$pendingMsgData = $reader->getUrlData('count_pending_messages', 'get', true);
$getMsgData = $reader->getUrlData('get_messages', 'get', true);
$friendProgData = $reader->getUrlData('get_friends_progress', 'get', true);
$friendScData = $reader->getUrlData('get_friends_score', 'get', true);
$usersGetData = $reader->getUrlData('users', 'get', true);
$usersPostData = $reader->getUrlData('users', 'post', true);

// display output
if ( php_sapi_name() !== 'cli' ) {
    echo "<h1>Pocket Playlab Challenge</h1>";
    echo "<h2>Output : </h2>";
}
echo $pendingMsgData . $separator . $separator;
echo $getMsgData . $separator . $separator;
echo $friendProgData . $separator . $separator;
echo $friendScData . $separator . $separator;
echo $usersGetData . $separator . $separator;
echo $usersPostData . $separator . $separator;